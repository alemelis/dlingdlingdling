import bpy

if __name__ == "__main__":
    # set scene
    scene = bpy.context.scene
    scene.render.engine = 'CYCLES'
    scene.world.use_nodes = True

    renderings_path = "./renderings/glossy_red_yellow_char/"

    for i in range(65, 91):
        character = chr(i)
        bpy.ops.font.delete(type='PREVIOUS_OR_SELECTION')
        bpy.ops.font.text_insert(text=character)

        rendering_filename = "{}.jpg".format(character)

        scene.render.filepath = renderings_path + rendering_filename
        bpy.ops.render.render(write_still=True)

